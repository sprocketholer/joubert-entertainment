import ApiService from "../Utilities/ApiService";

const FilmService = {};

FilmService.GetFilms = async () => {
    try {
        let response = await ApiService.get('/films');

        if (response.status === 200) {
            return response.data;
        } else {
            return [];
        }
    } catch (e) {
        console.log(e);
    }
}

export default FilmService;