import React, { Component } from "react";
import FilmService from "../../Services/FilmService";

import editIcon from '../../resources/images/edit.png';
import deleteIcon from '../../resources/images/delete.png';
import hollywoodSign from '../../resources/images/hollywood-sign.jpg';

import './FilmSelect.css'

class FilmSelect extends Component {

    constructor(props) {
        super(props);
        this.state = { films: [] };

    }

    componentDidMount = async () => {
        let films = await FilmService.GetFilms();
        this.setState({ films: films });

        window.$('[data-toggle="popover"]').popover();
    }

    newFilm = () => {
        alert("New film function not implemented.");
    }

    selectFilm = (film) => {
        alert("Edit film function not implemented.");
    }

    deleteFilm = (film) => {
        alert("Delete film function not implemented.")
    }

    getRows = (films) => {

        let rows = [];

        films.forEach((item, index) => {

            rows.push(
                <tr key={index}>
                    <td><img src={editIcon} alt={'edit'} title={ 'Edit ' + item.filmTitle } className={'edit-icon'} onClick={ () => this.selectFilm(item) }/></td>
                    <td>
                        <div className={'btn btn-link'} data-toggle={'popover'}
                             title={ item.filmTitle }
                             data-content={ item.logLine }>{ item.filmTitle }
                        </div>
                    </td>
                    <td>{ this.formatDate(item.releaseDate) }</td>
                    <td>{ this.formatMoney(item.budget) }</td>
                    <td>{ this.formatMoney(item.boxOffice) }</td>
                    <td><img src={deleteIcon} alt={'delete'} className={'delete-icon'}  title={ 'Delete ' + item.filmTitle } onClick={ () => this.deleteFilm(item) }/></td>
                </tr>
            )
        });

        return rows;
    }

    formatDate = (date) => {
        let releaseDate = new Date(date);
        let day = releaseDate.getDate();
        let month = releaseDate.getMonth() + 1;
        let year = releaseDate.getFullYear();

        if( day < 10)
        {
            day=`0${day}`;
        }

        if (month < 10)
        {
            month =`0${month}`;
        }

        return `${month}/${day}/${year}`
    }

    formatMoney = (num) => {
        let formattedNumber = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        return `$${formattedNumber}.00`;
    }

    render = () => {

        let rows = this.getRows(this.state.films);

        return (

            <div className={'container'}>
                <div className={'row pt-3'}>
                    <div className={'col align-center'}>
                            <img src={ hollywoodSign} alt={'Hollywood sign'} className={ 'hollywood-sign' }/>
                    </div>
                </div>
                <div className={'row pt-3'}>
                    <div className={'col align-right'}>

                        <div className={'btn btn-primary'} onClick={this.newFilm}>New</div>
                    </div>
                </div>
                <div className={'row pt-3'}>
                    <div className={'col'}>
                        <table className={'table'}>
                            <thead className={'thead-light'}>
                            <tr>
                                <th>&nbsp;</th>
                                <th scope='col'>Film</th>
                                <th scope='col'>Release Date</th>
                                <th scope='col'>Budget</th>
                                <th scope='col'>Box Office</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            {rows}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        );
    }
}

export default FilmSelect;