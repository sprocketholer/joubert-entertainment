import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactNotification from 'react-notifications-component';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';

import FilmSelect from "../FilmSelect/FilmSelect";
import FilmEdit from "../FilmEdit/FilmEdit";

function App() {
  return (
      <div className={'container'}>
        <ReactNotification />
        <div className={'row'}>
          <div className={'col'}>
            <Router>
              <Switch>
                <Route path="/film">
                  <div className={'row'}>
                    <div className={'col'}>
                      <FilmEdit/>
                    </div>
                  </div>
                </Route>
                <Route path="/">
                  <div className={'row'}>
                    <div className={'col'}>
                      <FilmSelect/>
                    </div>
                  </div>
                </Route>
              </Switch>
            </Router>
          </div>
        </div>
      </div>
  );
}

export default App;
