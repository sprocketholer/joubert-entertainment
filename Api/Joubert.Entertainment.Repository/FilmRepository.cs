// <copyright file="FilmRepository.cs" company="Stephen J. Joubert">
//    Copyright (c) Stephen J. Joubert All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Models;

    /// <summary>
    /// Film Repository class
    /// </summary>
    public class FilmRepository : RepositoryBase, IFilmRepository
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmRepository"/> class
        /// </summary>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public FilmRepository(ILogService logService) : base(logService)
        {
        }

        #endregion

        #region Public Methods

        /// <inheritdoc />
        public List<Film> Get()
        {
            this._logService.Debug("FilmRepository.Get called.");
            
            var sql = "SELECT " +
                "FilmId, FilmTitle, LogLine, ReleaseDate, Budget, BoxOffice " + 
                "FROM Film ORDER BY FilmTitle";
            
            List<Film> films = new List<Film>();
            
            this._logService.Verbose("FilmRepository.Get, SQL: {0}", sql);
            
            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    connection.Open();
                    
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            films.Add(this.GetFilm(dataReader));
                        }
                    }
                }
            }
            
            this._logService.Verbose("FilmRepository.Get returned {0} records.", films.Count);
            
            return films;
        }

        /// <inheritdoc />
        public Film GetSingle(Guid filmId)
        {
            this._logService.Debug("FilmRepository.GetSingle called.");

            if (filmId == Guid.Empty)
            {
                throw new ArgumentException(nameof(filmId));
            }
            
            var sql = "SELECT " +
                "FilmId, FilmTitle, LogLine, ReleaseDate, Budget, BoxOffice " + 
                "FROM Film " + 
                "WHERE FilmId = @FilmId";
            
            Film film = null;
            
            this._logService.Verbose("FilmRepository.GetSingle, SQL: {0}", sql);
            
            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    command.Parameters.AddWithValue("@FilmId", filmId);
                    
                    connection.Open();
                    
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.Read())
                        {
                            film = this.GetFilm(dataReader);
                        }
                    }
                }
            }
            
            if (film == null)
            {
                this._logService.Verbose("FilmRepository.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("FilmRepository.GetSingle returned 1 record.");
            }
            
            return film;
        }

        /// <inheritdoc />
        public bool Insert(Film film)
        {
            this._logService.Debug("FilmRepository.Insert called.");
            
            if (film == null)
            {
                throw new ArgumentNullException(nameof(film));
            }
            
            var sql = "INSERT INTO Film(FilmId, FilmTitle, LogLine, ReleaseDate, Budget, BoxOffice) " +
                "VALUES (@FilmId, @FilmTitle, @LogLine, @ReleaseDate, @Budget, @BoxOffice)";
            
            this._logService.Verbose("FilmRepository.Insert, SQL: {0}", sql);
            
            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    this.GetParameters(command, film);
                    
                    connection.Open();
                    
                    var result = command.ExecuteNonQuery();
                    
                    if (result == 1)
                    {
                        this._logService.Information("FilmRepository.Insert successful.");
                        return true;
                    }
                    else
                    {
                        this._logService.Verbose("FilmRepository.Insert Faile.");
                        return false;
                    }
                }
            }
        }

        /// <inheritdoc />
        public bool Update(Film film)
        {
            this._logService.Debug("FilmRepository.Update called.");
            
            if (film == null)
            {
                throw new ArgumentNullException(nameof(film));
            }
            
            var sql = "UPDATE Film SET " + 
                "FilmTitle = @FilmTitle " +
                ",LogLine = @LogLine " +
                ",ReleaseDate = @ReleaseDate " +
                ",Budget = @Budget " +
                ",BoxOffice = @BoxOffice " +
                "WHERE FilmId = @FilmId";
            
            this._logService.Verbose("FilmRepository.Update, SQL: {0}", sql);
            
            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    this.GetParameters(command, film);
                    
                    connection.Open();
                    
                    var result = command.ExecuteNonQuery();
                    
                    if (result == 1)
                    {
                        this._logService.Verbose("FilmRepository.Update successful.");
                        return true;
                    }
                    else
                    {
                        this._logService.Verbose("FilmRepository.Update Faile.");
                        return false;
                    }
                }
            }
        }

        /// <inheritdoc />
        public bool Delete(Guid filmId)
        {
            this._logService.Debug("FilmRepository.Delete called.");

            if (filmId == Guid.Empty)
            {
                throw new ArgumentException(nameof(filmId));
            }
            
            var sql = "DELETE FROM Film " + 
                "WHERE FilmId = @FilmId";
            
            this._logService.Verbose("FilmRepository.Delete, SQL: {0}", sql);
            
            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    command.Parameters.AddWithValue("@FilmId", filmId);
                    
                    connection.Open();
                    
                    var result = command.ExecuteNonQuery();
                    
                    if (result == 1)
                    {
                        this._logService.Verbose("FilmRepository.Delete successful.");
                        return true;
                    }
                    else
                    {
                        this._logService.Verbose("FilmRepository.Delete Faile.");
                        return false;
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Converts a data reader record into a Film
        /// </summary>
        /// <param name="dataReader">An instance of the <see cref="SqlDataReader"/> class</param>
        /// <returns>A list of <see cref="Film"/> instances</returns>
        private Film GetFilm(SqlDataReader dataReader)
        {
            return new Film()
            {
                FilmId = (Guid)dataReader["FilmId"],
                FilmTitle = (string)dataReader["FilmTitle"],
                LogLine = (string)dataReader["LogLine"],
                ReleaseDate = (DateTime)dataReader["ReleaseDate"],
                Budget = (decimal)dataReader["Budget"],
                BoxOffice = (decimal?)(dataReader.IsDBNull(5) ? null : dataReader["BoxOffice"]),
            };
        }

        /// <summary>
        /// Converts a Film into SQL Parameters 
        /// </summary>
        /// <param name="command">An instance of the <see cref="SqlCommand"/> class</param>
        /// <param name="film">An instance of the <see cref="Film"/> class</param>
        /// <returns>An instance of the <see cref="SqlCommand"/> class with parameters added</returns>
        private SqlCommand GetParameters(SqlCommand command, Film film)
        {
            command.Parameters.AddWithValue("@FilmId", film.FilmId);
            command.Parameters.AddWithValue("@FilmTitle", film.FilmTitle);
            command.Parameters.AddWithValue("@LogLine", film.LogLine);
            command.Parameters.AddWithValue("@ReleaseDate", film.ReleaseDate);
            command.Parameters.AddWithValue("@Budget", film.Budget);
            command.Parameters.AddWithValue("@BoxOffice", film.BoxOffice == null ? DBNull.Value : (object)film.BoxOffice);

            return command;
        }

        #endregion
    }
}
