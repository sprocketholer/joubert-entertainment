﻿// <copyright file="FilmControllerTests.cs" company="Stephen J. Joubert">
//     Copyright(c) Stephen J. Joubert. All rights reserved.
// </copyright >

namespace Joubert.Entertainment.Api.Tests
{
    using System;
    using System.Collections.Generic;
    using Api.Controllers;
    using Common;
    using Common.Interfaces.Service;
    using Common.Models;
    using Microsoft.AspNetCore.Mvc;
    using NSubstitute;
    using Xunit;

    /// <summary>
    /// Film controller unit tests
    /// </summary>
    public class FilmControllerTests : ControllerTestBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmControllerTests"/> class
        /// </summary>
        public FilmControllerTests()
        {
            var logService = this.GetLogServiceMock();

            this._film = new Film()
            {
                FilmId = Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec"),
                FilmTitle = "Empire of the Sun",
                LogLine = "English boy in WWII",
                ReleaseDate = DateTime.Parse("1987-12-25"),
                Budget = 35000000.0M,
                BoxOffice = 22238696.0M
            };

            Film filmNull = null;

            var films = new List<Film>() { new Film(), new Film(), new Film() };

            var serviceResultSuccess = new ServiceResult<Film>() { IsSuccessful = true };
            var serviceResultFail = new ServiceResult<Film>() { IsSuccessful = false };

            var filmService = Substitute.For<IFilmService>();
            filmService.Get().Returns(films);
            filmService.GetSingle(Arg.Any<Guid>()).Returns(filmNull);
            filmService.GetSingle(Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec")).Returns(this._film);
            filmService.Insert(Arg.Any<Film>()).Returns(serviceResultFail);
            filmService.Insert(this._film).Returns(serviceResultSuccess);
            filmService.Update(Arg.Any<Film>()).Returns(serviceResultFail);
            filmService.Update(this._film).Returns(serviceResultSuccess);
            filmService.Delete(Arg.Any<Guid>()).Returns(serviceResultFail);
            filmService.Delete(Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec")).Returns(serviceResultSuccess);

            this._filmController = new FilmController(filmService, logService);
        }

        #endregion

        #region Private Properties

        /// <summary>
        ///  Gets an instance of the <see cref="FilmController"/> class
        /// </summary>
        private FilmController _filmController { get; }

        /// <summary>
        /// Gets an instance of the <see cref="Film"/> class
        /// </summary>
        private Film _film { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Tests Get - Success
        /// </summary>
        [Fact]
        public void FilmService_Get_Success()
        {
            var result = (ObjectResult)this._filmController.Get();

            Assert.Equal(200, result.StatusCode);
        }

        /// <summary>
        /// Tests GetSingle - Success
        /// </summary>
        [Fact]
        public void FilmService_GetSingle_Success()
        {
            var result = (ObjectResult)this._filmController.GetSingle(Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec"));

            Assert.Equal(200, result.StatusCode);
        }

        /// <summary>
        /// Tests GetSingle - Fail
        /// </summary>
        [Fact]
        public void FilmService_GetSingle_Fail()
        {
            var result = (ObjectResult)this._filmController.GetSingle(Guid.Parse("286b78eb-1fd2-47ab-a18c-8dd9da2132f0"));

            Assert.Equal(200, result.StatusCode);
        }

        /// <summary>
        /// Tests GetSingle - Exception
        /// </summary>
        [Fact]
        public void FilmService_GetSingle_Exception()
        {
            var result = (StatusCodeResult)this._filmController.GetSingle(Guid.Empty);

            Assert.Equal(422, result.StatusCode);
        }

        /// <summary>
        /// Tests Insert - Success
        /// </summary>
        [Fact]
        public void FilmService_Insert_Success()
        {
            var result = (ObjectResult)this._filmController.Post(this._film);

            Assert.Equal(201, result.StatusCode);
        }

        /// <summary>
        /// Tests Insert - Fail
        /// </summary>
        [Fact]
        public void FilmService_Insert_Fail()
        {
            var result = (ObjectResult)this._filmController.Post(new Film());

            Assert.Equal(422, result.StatusCode);
        }

        /// <summary>
        /// Tests Insert - Exception
        /// </summary>
        [Fact]
        public void FilmService_Insert_Exception()
        {
            var result = (StatusCodeResult)this._filmController.Post(null);

            Assert.Equal(422, result.StatusCode);
        }

        /// <summary>
        /// Tests Update - Success
        /// </summary>
        [Fact]
        public void FilmService_Update_Success()
        {
            var result = (ObjectResult)this._filmController.Put(this._film);

            Assert.Equal(200, result.StatusCode);
        }

        /// <summary>
        /// Tests Update - Fail
        /// </summary>
        [Fact]
        public void FilmService_Update_Fail()
        {
            var result = (ObjectResult)this._filmController.Put(new Film());

            Assert.Equal(422, result.StatusCode);
        }

        /// <summary>
        /// Tests Update - Exception
        /// </summary>
        [Fact]
        public void FilmService_Update_Exception()
        {
            var result = (StatusCodeResult)this._filmController.Put(null);

            Assert.Equal(422, result.StatusCode);
        }

        /// <summary>
        /// Tests Delete - Success
        /// </summary>
        [Fact]
        public void FilmService_Delete_Success()
        {
            var result = (ObjectResult)this._filmController.Delete(Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec"));

            Assert.Equal(200, result.StatusCode);
        }

        /// <summary>
        /// Tests Delete - Fail
        /// </summary>
        [Fact]
        public void FilmService_Delete_Fail()
        {
            var result = (ObjectResult)this._filmController.Delete(Guid.Parse("286b78eb-1fd2-47ab-a18c-8dd9da2132f0"));

            Assert.Equal(200, result.StatusCode);
        }

        /// <summary>
        /// Tests Delete - Exception
        /// </summary>
        [Fact]
        public void FilmService_Delete_Exception()
        {
            var result = (StatusCodeResult)this._filmController.Delete(Guid.Empty);

            Assert.Equal(422, result.StatusCode);
        }

        #endregion
    }
}
