﻿// <copyright file="FilmRepositoryTests.cs" company="Stephen J. Joubert">
//     Copyright(c) Stephen J. Joubert. All rights reserved.
// </copyright >

namespace Joubert.Entertainment.Repository.Tests
{
    using System;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices.ComTypes;
    using Joubert.Entertainment.Common.Models;
    using NSubstitute.Core;
    using NSubstitute.Core.SequenceChecking;
    using Repository;
    using SqlTestPrep;
    using Xunit;

    /// <summary>
    /// Film repository unit tests
    /// </summary>
    [Collection("Sequential")]
    public class FilmRepositoryTests : RepositoryTestBase, IDisposable
    {
        #region Constructors
        
        /// <summary>
        /// Initializes a new instance of the <see cref="FilmRepositoryTests"/> class
        /// </summary>
        public FilmRepositoryTests()
        {
            this._sqlLoader = new SqlLoader();
            var logService = this.GetLogServiceMock();

            this._filmRepository = new FilmRepository(logService);

            try
            {
                this._sqlLoader.Setup("Entertainment");
            }
            catch 
            {
                this._sqlLoader.TearDown("Entertainment");
            }
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an instance of the <see cref="SqlLoader"/> class
        /// </summary>
        private SqlLoader _sqlLoader { get; }

        /// <summary>
        /// Gets and instance of the <see cref="FilmRepository"/> class
        /// </summary>
        private FilmRepository _filmRepository { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Tests Get - Success
        /// </summary>
        [Fact]
        public void FilmRepository_Get_Success()
        {
            var result = this._filmRepository.Get();

            Assert.NotNull(result);
            Assert.Equal(3, result.Count);
        }

        /// <summary>
        /// Tests GetSingle - Success
        /// </summary>
        [Fact]
        public void FilmRepository_GetSingle_Success()
        {
            var filmId = Guid.Parse("C5CF828E-0D0F-4DE0-93FD-2EF9666E50D9");

            var result = this._filmRepository.GetSingle(filmId);

            Assert.NotNull(result);
            Assert.Equal(filmId, result.FilmId);
        }

        /// <summary>
        /// Tests GetSingle - Fail
        /// </summary>
        [Fact]
        public void FilmRepository_GetSingle_Fail()
        {
            var filmId = Guid.Parse("43d6b3f0-a019-49e9-a78b-57c979170d6a");

            var result = this._filmRepository.GetSingle(filmId);

            Assert.Null(result);
        }

        /// <summary>
        /// Tests GetSingle - Exception
        /// </summary>
        [Fact]
        public void FilmRepository_GetSingle_Exception()
        {
            Assert.Throws<ArgumentException>(() => this._filmRepository.GetSingle(Guid.Empty));
        }

        /// <summary>
        /// Tests Insert - Success
        /// </summary>
        [Fact]
        public void FilmRepository_Insert_Success()
        {
            var film = new Film()
            {
                FilmId = Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec"),
                FilmTitle = "Empire of the Sun",
                LogLine = "English boy in WWII",
                ReleaseDate = DateTime.Parse("1987-12-25"),
                Budget = 35000000.0M
            };

            var result = this._filmRepository.Insert(film);

            Assert.True(result);
        }

        /// <summary>
        /// Tests Insert - SQL Exception
        /// </summary>
        [Fact]
        public void FilmRepository_Insert_SqlException()
        {
            var film = new Film()
            {
                FilmId = Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec"),
                FilmTitle = "Empire of the Sun",
                ReleaseDate = DateTime.Parse("1987-12-25"),
                Budget = 35000000.0M
            };

            Assert.Throws<SqlException>(() => this._filmRepository.Insert(film));
        }

        /// <summary>
        /// Tests Insert - Exception
        /// </summary>
        [Fact]
        public void FilmRepository_Insert_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => this._filmRepository.Insert(null));
        }

        /// <summary>
        /// Tests Update - Success
        /// </summary>
        [Fact]
        public void FilmRepository_Update_Success()
        {
            var film = new Film()
            {
                FilmId = Guid.Parse("C5CF828E-0D0F-4DE0-93FD-2EF9666E50D9"),
                FilmTitle = "Empire of the Sun",
                LogLine = "English boy in WWII",
                ReleaseDate = DateTime.Parse("1987-12-25"),
                Budget = 35000000.0M
            };

            var result = this._filmRepository.Update(film);

            Assert.True(result);
        }

        /// <summary>
        /// Tests Update - SQL Exception
        /// </summary>
        [Fact]
        public void FilmRepository_Update_SqlException()
        {
            var film = new Film()
            {
                FilmId = Guid.Parse("C5CF828E-0D0F-4DE0-93FD-2EF9666E50D9"),
                FilmTitle = "Empire of the Sun",
                ReleaseDate = DateTime.Parse("1987-12-25"),
                Budget = 35000000.0M
            };

            Assert.Throws<SqlException>(() => this._filmRepository.Update(film));
        }

        /// <summary>
        /// Tests Update - Exception
        /// </summary>
        [Fact]
        public void FilmRepository_Update_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => this._filmRepository.Update(null));
        }

        /// <summary>
        /// Tests Delete - Success
        /// </summary>
        [Fact]
        public void FilmRepository_Delete_Success()
        {
            var filmId = Guid.Parse("C5CF828E-0D0F-4DE0-93FD-2EF9666E50D9");

            var result = this._filmRepository.Delete(filmId);

            Assert.True(result);
        }

        /// <summary>
        /// Tests Delete - Fail
        /// </summary>
        [Fact]
        public void FilmRepository_Delete_Fail()
        {
            var filmId = Guid.Parse("43d6b3f0-a019-49e9-a78b-57c979170d6a");

            var result = this._filmRepository.Delete(filmId);

            Assert.False(result);
        }

        /// <summary>
        /// Tests Delete - Exception
        /// </summary>
        [Fact]
        public void FilmRepository_Delete_Exception()
        {
            Assert.Throws<ArgumentException>(() => this._filmRepository.Delete(Guid.Empty));
        }

        /// <summary>
        /// Dispose method to clean up the database
        /// </summary>
        public void Dispose()
        {
            this._sqlLoader.TearDown("Entertainment");
        }

        #endregion
    }
}
