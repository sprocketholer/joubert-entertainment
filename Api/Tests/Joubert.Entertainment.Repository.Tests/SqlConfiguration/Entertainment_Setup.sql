﻿INSERT INTO Film (FilmId, FilmTitle, LogLine, ReleaseDate, Budget, BoxOffice)
VALUES ('a1161a09-2ffa-4779-8686-7333032006f2', 'Jojo Rabbit', 'A young boy in Hitler''s army finds out his mother is hiding a Jewish girl in their home.', '2019-11-08', 14000000.0, 90335025.0)

INSERT INTO Film (FilmId, FilmTitle, LogLine, ReleaseDate, Budget, BoxOffice)
VALUES ('c5cf828e-0d0f-4de0-93fd-2ef9666e50d9', 'Lost in Translation', 'A faded movie star and a neglected young woman form an unlikely bond after crossing paths in Tokyo.', '2003-10-03', 4000000.0, 118685453.0)

INSERT INTO Film (FilmId, FilmTitle, LogLine, ReleaseDate, Budget, BoxOffice)
VALUES ('911a4800-426f-45d1-91de-014faa76096c', 'Raider of the Lost Ark', 'In 1936, archaeologist and adventurer Indiana Jones is hired by the U.S. government to find the Ark of the Covenant before Adolf Hitler''s Nazis can obtain its awesome powers.', '1981-06-12', 18000000.0, 390133212.0)