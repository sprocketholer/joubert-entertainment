﻿// <copyright file="RepositoryTestBase.cs" company="Stephen J. Joubert">
//     Copyright(c) Stephen J. Joubert. All rights reserved.
// </copyright >

namespace Joubert.Entertainment.Repository.Tests
{
    using Common.Interfaces.Logging;
    using NSubstitute;

    /// <summary>
    /// Repository test base (abstract)
    /// </summary>
    public abstract class RepositoryTestBase
    {
        #region Protected Methods

        /// <summary>
        /// Builds a mock of the ILogWriter interface
        /// </summary>
        /// <returns>An implementation of the <see cref="ILogWriter"/> interface</returns>
        protected ILogService GetLogServiceMock()
        {
            var logWriter = Substitute.For<ILogService>();

            logWriter.Verbose(Arg.Any<string>());
            logWriter.Verbose(Arg.Any<string>(), Arg.Any<object[]>());

            logWriter.Debug(Arg.Any<string>());
            logWriter.Debug(Arg.Any<string>(), Arg.Any<object[]>());

            logWriter.Information(Arg.Any<string>());
            logWriter.Information(Arg.Any<string>(), Arg.Any<object[]>());

            logWriter.Warning(Arg.Any<string>());
            logWriter.Warning(Arg.Any<string>(), Arg.Any<object[]>());

            logWriter.Error(Arg.Any<string>());
            logWriter.Error(Arg.Any<string>(), Arg.Any<object[]>());

            logWriter.Fatal(Arg.Any<string>());
            logWriter.Fatal(Arg.Any<string>(), Arg.Any<object[]>());

            return logWriter;
        }

        #endregion
    }
}
