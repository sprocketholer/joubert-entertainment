﻿// <copyright file="FilmServiceTests.cs" company="Stephen J. Joubert">
//     Copyright(c) Stephen J. Joubert. All rights reserved.
// </copyright >

namespace Joubert.Entertainment.Service.Tests
{
    using System;
    using Common.Models;
    using Joubert.Entertainment.Common.Interfaces.Repository;
    using NSubstitute;
    using Service;
    using Xunit;

    /// <summary>
    /// Film service unit tests
    /// </summary>
    public class FilmServiceTests : ServiceTestBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmServiceTests"/> class
        /// </summary>
        public FilmServiceTests()
        {
            var logService = this.GetLogServiceMock();

            this._film = new Film()
            {
                FilmId = Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec"),
                FilmTitle = "Empire of the Sun",
                LogLine = "English boy in WWII",
                ReleaseDate = DateTime.Parse("1987-12-25"),
                Budget = 35000000.0M,
                BoxOffice = 22238696.0M
            };

            Film filmNull = null;

            var filmRepository = Substitute.For<IFilmRepository>();
            filmRepository.GetSingle(Arg.Any<Guid>()).Returns(filmNull);
            filmRepository.GetSingle(Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec")).Returns(this._film);
            filmRepository.Insert(Arg.Any<Film>()).Returns(false);
            filmRepository.Insert(this._film).Returns(true);
            filmRepository.Update(Arg.Any<Film>()).Returns(false);
            filmRepository.Update(this._film).Returns(true);
            filmRepository.Delete(Arg.Any<Guid>()).Returns(false);
            filmRepository.Delete(Guid.Parse("7fae7a69-9ea6-459c-a2d4-ced0f26895ec")).Returns(true);

            this._filmService = new FilmService(filmRepository, logService);
        }

        #endregion

        #region Private Properties

        /// <summary>
        ///  Gets an instance of the <see cref="FilmService"/> class
        /// </summary>
        private FilmService _filmService { get; }

        /// <summary>
        ///  Gets an instance of the <see cref="Film"/> class
        /// </summary>
        private Film _film { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Tests GetSingle - Exception
        /// </summary>
        [Fact]
        public void FilmService_GetSingle_Exception()
        {
            Assert.Throws<ArgumentException>(() => this._filmService.GetSingle(Guid.Empty));
        }

        /// <summary>
        /// Tests Insert - Success
        /// </summary>
        [Fact]
        public void FilmService_Insert_Success()
        {
            var result = this._filmService.Insert(this._film);

            Assert.NotNull(result);
            Assert.True(result.IsValid);
            Assert.True(result.IsSuccessful);
        }

        /// <summary>
        /// Tests Insert - Exception
        /// </summary>
        [Fact]
        public void FilmService_Insert_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => this._filmService.Insert(null));
        }

        /// <summary>
        /// Tests Update - Success
        /// </summary>
        [Fact]
        public void FilmService_Update_Success()
        {
            var result = this._filmService.Update(this._film);

            Assert.NotNull(result);
            Assert.True(result.IsValid);
            Assert.True(result.IsSuccessful);
        }

        /// <summary>
        /// Tests Update - Exception
        /// </summary>
        [Fact]
        public void FilmService_Update_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => this._filmService.Insert(null));
        }

        /// <summary>
        /// Tests Validation Film Id - Fail
        /// </summary>
        [Fact]
        public void FilmService_Validation_FilmId_Fail()
        {
            this._film.FilmId = Guid.Empty;

            var result = this._filmService.Insert(this._film);

            Assert.NotNull(result);
            Assert.False(result.IsValid);
            Assert.False(result.IsSuccessful);
            Assert.NotEmpty(result.Messages);
        }

        /// <summary>
        /// Tests Validation Film Title - Fail
        /// </summary>
        [Fact]
        public void FilmService_Validation_FilmTitle_Fail()
        {
            this._film.FilmTitle = null;

            var result = this._filmService.Insert(this._film);

            Assert.NotNull(result);
            Assert.False(result.IsValid);
            Assert.False(result.IsSuccessful);
            Assert.NotEmpty(result.Messages);
        }

        /// <summary>
        /// Tests Validation Film Title - Fail
        /// </summary>
        [Fact]
        public void FilmService_Validation_FilmTitle_Fail2()
        {
            this._film.FilmTitle = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";

            var result = this._filmService.Insert(this._film);

            Assert.NotNull(result);
            Assert.False(result.IsValid);
            Assert.False(result.IsSuccessful);
            Assert.NotEmpty(result.Messages);
        }

        /// <summary>
        /// Tests Validation Log Line - Fail
        /// </summary>
        [Fact]
        public void FilmService_Validation_LogLine_Fail()
        {
            this._film.LogLine = null;

            var result = this._filmService.Insert(this._film);

            Assert.NotNull(result);
            Assert.False(result.IsValid);
            Assert.False(result.IsSuccessful);
            Assert.NotEmpty(result.Messages);
        }

        /// <summary>
        /// Tests Delete - Exception
        /// </summary>
        [Fact]
        public void FilmService_Delete_Exception()
        {
            Assert.Throws<ArgumentException>(() => this._filmService.Delete(Guid.Empty));
        }

        #endregion
    }
}
