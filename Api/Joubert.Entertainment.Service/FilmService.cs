// <copyright file="FilmService.cs" company="Stephen J. Joubert">
//    Copyright (c) Stephen J. Joubert All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Service
{
    using System;
    using System.Collections.Generic;
    using Common;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;
    using FluentValidation;

    /// <summary>
    /// Film Service class
    /// </summary>
    public class FilmService : ServiceBase, IFilmService
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmService"/> class
        /// </summary>
        /// <param name="filmRepository">An implementation of the <see cref="IFilmRepository"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public FilmService(IFilmRepository filmRepository, ILogService logService) : base(logService)
        {
            this._filmRepository = filmRepository;
            this._filmValidator = new FilmValidator();
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an implementation of the <see cref="IFilmRepository"/> interface
        /// </summary>
        private IFilmRepository _filmRepository { get; }

        /// <summary>
        /// Gets an instance of the <see cref="FilmValidator"/> class
        /// </summary>
        private FilmValidator _filmValidator { get; }

        #endregion

        #region Public Methods

        /// <inheritdoc />
        public List<Film> Get()
        {
            this._logService.Debug("FilmService.Get called.");
            
            var films = new List<Film>();
            
            films = this._filmRepository.Get();
            
            this._logService.Verbose("FilmService.Get returned {0} records.", films.Count);
            
            return films;
        }

        /// <inheritdoc />
        public Film GetSingle(Guid filmId)
        {
            this._logService.Debug("FilmService.GetSingle called.");

            if (filmId == Guid.Empty)
            {
                throw new ArgumentException(nameof(filmId));
            }

            var film = this._filmRepository.GetSingle(filmId);

            if (film == null)
            {
                this._logService.Verbose("FilmService.GetSingle returned 0 records.");
            }
            else
            {
                this._logService.Verbose("FilmService.GetSingle returned 1 record.");
            }

            return film;
        }

        /// <inheritdoc />
        public ServiceResult<Film> Insert(Film film)
        {
            if (film == null)
            {
                throw new ArgumentNullException(nameof(film));
            }

            this._logService.Debug("FilmService.Insert called.");

            var serviceResult = this._filmValidator.ValidateFilm(film);

            if (!serviceResult.IsValid)
            {
                this._logService.Verbose("FilmService.Insert failed validation.");
                return serviceResult;
            }

            this._logService.Verbose("FilmService.Insert passed validation.");

            serviceResult.IsSuccessful = this._filmRepository.Insert(film);

            if (serviceResult.IsSuccessful)
            {
                this._logService.Verbose("FilmService.Insert succeeded.");
            }
            else
            {
                this._logService.Verbose("FilmService.Insert failed.");
            }

            return serviceResult;
        }

        /// <inheritdoc />
        public ServiceResult<Film> Update(Film film)
        {
            if (film == null)
            {
                throw new ArgumentNullException(nameof(film));
            }

            this._logService.Debug("FilmService.Update called.");

            var serviceResult = this._filmValidator.ValidateFilm(film);

            if (!serviceResult.IsValid)
            {
                this._logService.Verbose("FilmService.Update failed validation.");
                return serviceResult;
            }

            this._logService.Verbose("FilmService.Update passed validation.");

            serviceResult.IsSuccessful = this._filmRepository.Update(film);

            if (serviceResult.IsSuccessful)
            {
                this._logService.Verbose("FilmService.Update succeeded.");
            }
            else
            {
                this._logService.Verbose("FilmService.Update failed.");
            }

            return serviceResult;
        }

         /// <inheritdoc/>
        public ServiceResult<Film> Delete(Guid filmId)
        {
            this._logService.Debug("FilmService.Delete called.");

            if (filmId == Guid.Empty)
            {
                throw new ArgumentException(nameof(filmId));
            }

            var serviceResult = new ServiceResult<Film>();

            serviceResult.IsSuccessful = this._filmRepository.Delete(filmId);

            if (serviceResult.IsSuccessful)
            {
                this._logService.Verbose("FilmService.Delete succeeded.");
            }
            else
            {
                this._logService.Verbose("FilmService.Delete failed.");
            }

            return serviceResult;
        }

        #endregion

        #region Internal Classes

        /// <summary>
        /// Film validator (Internal)
        /// </summary>
        internal class FilmValidator : AbstractValidator<Film>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="FilmValidator"/> class
            /// </summary>
            public FilmValidator()
            {
                RuleFor(f => f.FilmId).NotNull().NotEmpty();
                RuleFor(f => f.FilmTitle).NotNull().NotEmpty().MaximumLength(100);
                RuleFor(f => f.LogLine).NotNull().NotEmpty().MaximumLength(4000);
            }
        
            /// <summary>
            /// Validates a Film
            /// </summary>
            /// <param name="film">An instance of the <see cref="film"/> class</param>
            /// <returns>An instance of the <see cref="ServiceResult{Film}"/> class</returns>
            public ServiceResult<Film> ValidateFilm(Film film)
            {
                var serviceResult = new ServiceResult<Film>()
                {
                    Object = film
                };
        
                var result = this.Validate(film);
        
                serviceResult.IsValid = result.IsValid;
        
                if (!result.IsValid)
                {
                    foreach (var error in result.Errors)
                    {
                        serviceResult.Messages.Add(new Message() { MessageText = error.ErrorMessage, FieldName = error.PropertyName });
                    }
                }
        
                return serviceResult;
            }
        }

        #endregion
    }
}
