// <copyright file="Program.cs" company="Stephen J. Joubert">
//     Copyright (c)Stephen J. Joubert. All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Api
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting; 

    /// <summary>
    /// Program class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point for the application
        /// </summary>
        /// <param name="args">Array of arguments passed to the application on start up</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Provisions the host builder
        /// </summary>
        /// <param name="args">Array of arguments</param>
        /// <returns>An implementation of the <see cref="IHostBuilder"/> interface</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
