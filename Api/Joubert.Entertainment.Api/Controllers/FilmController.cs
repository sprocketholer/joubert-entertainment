// <copyright file="FilmController.cs" company="Stephen J. Joubert">
//     Copyright(c) Stephen J. Joubert. All rights reserved.
// </copyright >

namespace Joubert.Entertainment.Api.Controllers
{
    using System;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Service;
    using Common.Logging;
    using Common.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Film Controller
    /// </summary>
    [Route("Films")]
    [ApiController]
    public class FilmController : LoggingController
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmController"/> class
        /// </summary>
        /// <param name="filmService">An implementation of the <see cref="IFilmService"/> interface</param>
        /// <param name="logService">An implementation of the <see cref="ILogService"/> interface</param>
        public FilmController(IFilmService filmService, ILogService logService) : base(logService)
        {
            this._filmService = filmService;
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an implementation of the <see cref="IFilmService"/> interface
        /// </summary>
        private IFilmService _filmService { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets a list of all Films
        /// </summary>
        /// <returns>An <see cref="ActionResult"/> of a List of instances of the <see cref="Film"/> class</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                this._logService.Debug("FilmController.GetAll called.");

                var films = this._filmService.Get();

                this._logService.Verbose("FilmController.GetAll returned Film record/s.", films.Count);

                return this.StatusCode(200, films);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(422);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Gets a Film
        /// </summary>
        /// <param name="filmId">The Film Id</param>
        /// <returns>>An <see cref="ActionResult"/> of an instance of the <see cref="Film"/> class</returns>
        [HttpGet("{filmId}")]
        public ActionResult GetSingle(Guid filmId)
        {
            try
            {
                if (filmId == Guid.Empty)
                {
                    throw new ArgumentException(nameof(filmId));
                }

                this._logService.Debug("FilmController.Get called.");

                var film = this._filmService.GetSingle(filmId);

                if (film == null)
                {
                    this._logService.Verbose("FilmController.Get returned 0 records.");
                }
                else
                {
                    this._logService.Verbose("FilmController.Get returned 1 record.");
                }

                return this.StatusCode(200, film);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(422);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Inserts a new Film record
        /// </summary>
        /// <param name="film">An instance of the <see cref="Film"/> class</param>
        /// <returns>An instance of an <see cref="ActionResult"/> of an instance of the <see cref="ServiceResult{Film}"/> class</returns>
        [HttpPost]
        public ActionResult Post(Film film)
        {
            try
            {
                if (film == null)
                {
                    throw new ArgumentNullException(nameof(film));
                }

                this._logService.Debug("Film Controller.Post called.");

                var serviceResult = this._filmService.Insert(film);

                if (serviceResult.IsSuccessful)
                {
                    this._logService.Verbose("FilmController.Post succeeded.");
                    return this.StatusCode(201, serviceResult);
                }
                else
                {
                    this._logService.Verbose("FilmController.Post failed.");
                    return this.StatusCode(422, serviceResult);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(422);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Updates a Film record
        /// </summary>
        /// <param name="film">An instance of the <see cref="Film"/> class</param>
        /// <returns>An instance of an <see cref="ActionResult"/> of an instance of the <see cref="ServiceResult{Film}"/> class</returns>
        [HttpPut("{filmId}")]
        public ActionResult Put(Film film)
        {
            try
            {
                if (film == null)
                {
                    throw new ArgumentNullException(nameof(film));
                }

                this._logService.Debug("Film Controller.Put called.");

                var serviceResult = this._filmService.Update(film);

                if (serviceResult.IsSuccessful)
                {
                    this._logService.Verbose("FilmController.Put succeeded.");
                    return this.StatusCode(200, serviceResult);
                }
                else
                {
                    this._logService.Verbose("FilmController.Put failed.");
                    return this.StatusCode(422, serviceResult);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentNullException));
                return this.StatusCode(422);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Deletes a Film
        /// </summary>
        /// <param name="filmId">The Film Id</param>
        /// <returns>>An <see cref="ActionResult"/> of an instance of the <see cref="Film"/> class</returns>
        [HttpDelete("{filmId}")]
        public ActionResult Delete(Guid filmId)
        {
            try
            {
                if (filmId == Guid.Empty)
                {
                    throw new ArgumentException(nameof(filmId));
                }

                this._logService.Debug("FilmController.Delete called.");

                var serviceResult = this._filmService.Delete(filmId);

                if (serviceResult.IsSuccessful == true)
                {
                    this._logService.Verbose("FilmController.Delete successful.");
                }
                else
                {
                    this._logService.Verbose("FilmController.Delete Failed.");
                }

                return this.StatusCode(200, serviceResult);
            }
            catch (ArgumentException argumentException)
            {
                this._logService.Error(LogBuilder.GetLog(argumentException));
                return this.StatusCode(422);
            }
            catch (Exception exception)
            {
                this._logService.Error(LogBuilder.GetLog(exception));
                return this.StatusCode(500);
            }
        }

        #endregion
    }
}
