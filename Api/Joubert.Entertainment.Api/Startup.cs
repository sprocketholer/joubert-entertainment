// <copyright file="Startup.cs" company="Stephen J. Joubert">
//     Copyright (c) Stephen J. Joubert. All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Api
{
    using Common.Interfaces.Logging;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Logging;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;
    using Repository;
    using Service;

    /// <summary>
    /// Startup class
    /// </summary>
    public class Startup
    {
        #region Private Fields

        /// <summary>
        /// Name for origin
        /// </summary>
        private readonly string allowAnyOrigin = "allowAnyOrigin";

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class
        /// </summary>
        /// <param name="configuration">An implementation of the <see cref="IConfiguration"/> interface</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        #endregion

        #region public Properties

        /// <summary>
        /// Gets an implementation of the <see cref="IConfiguration"/> interface
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">An implementation of the <see cref="IServiceCollection"/> interface</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Common Configuration
            services.AddTransient<ILogService, LogService>();

            // Repository Configuration
            services.AddTransient<IFilmRepository, FilmRepository>();

            // Service Configuration
            services.AddTransient<IFilmService, FilmService>();

            // Set up CORS
            services.AddCors(options =>
            {
                options.AddPolicy(
                    name: this.allowAnyOrigin,
                    builder =>
                    {
                        builder.AllowAnyOrigin();
                        builder.AllowAnyHeader();
                        builder.AllowAnyMethod();
                    });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(
                    "v1",
                    new OpenApiInfo
                    {
                        Title = "Entertainment",
                        Version = "v1",
                        Description = "Entertainment API"
                    });
            });

            services.AddControllers();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">An implementation of the <see cref="IApplicationBuilder"/> interface</param>
        /// <param name="env">An implementation of the <see cref="IWebHostEnvironment"/> interface</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Entertainment API v1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(this.allowAnyOrigin);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        #endregion
    }
}
