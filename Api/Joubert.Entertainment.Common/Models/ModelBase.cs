// <copyright file="ModelBase.cs" company="Stephen J. Joubert">
//     Copyright (c)Stephen J. Joubert. All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Common.Models
{
    using System.Text.Json;

    /// <summary>
    /// Model base (abstract)
    /// </summary>
    /// <typeparam name="T">A class</typeparam>
    public abstract class ModelBase<T> where T : class
    {
        /// <summary>
        /// Static method that converts JSON to an instance of T
        /// </summary>
        /// <param name="json">The JSON to be converted</param>
        /// <returns>An instance of T</returns>
        public static T Parse(string json)
        {
            return JsonSerializer.Deserialize<T>(json);
        }

        /// <summary>
        /// Serializes the object as JSON
        /// </summary>
        /// <returns>A JSON representation of the object</returns>
        public string ToJson()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
