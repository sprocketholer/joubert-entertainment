// <copyright file="Film.cs" company="Stephen J. Joubert">
//     Copyright (c) Stephen J. Joubert All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Common.Models
{
    using System;

    /// <summary>
    /// Film class
    /// </summary>
    public class Film : ModelBase<Film>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Film"/> class
        /// </summary>
        public Film()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Film Id
        /// </summary>
        public Guid FilmId { get; set; }

        /// <summary>
        /// Gets or sets the Film Title
        /// </summary>
        public string FilmTitle { get; set; }

        /// <summary>
        /// Gets or sets the Log Line
        /// </summary>
        public string LogLine { get; set; }

        /// <summary>
        /// Gets or sets the Release Date
        /// </summary>
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Gets or sets the Budget
        /// </summary>
        public decimal Budget { get; set; }

        /// <summary>
        /// Gets or sets the Box Office
        /// </summary>
        public decimal? BoxOffice { get; set; }

        #endregion
    }
}
