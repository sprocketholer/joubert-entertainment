// <copyright file="IFilmService.cs" company="Stephen J. Joubert">
//    Copyright (c) Stephen J. Joubert All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Common.Interfaces.Service
{
    using System;
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// Film Service interface
    /// </summary>
    public interface IFilmService
    {
        /// <summary>
        /// Gets all Film records
        /// </summary>
        /// <returns>A list of <see cref='Film'/> instances</returns>
        List<Film> Get();

        /// <summary>
        /// Gets a Film record
        /// </summary>
        /// <param name="filmId">The Film Id</param>
        /// <returns>An instance of the <see cref='Film'/> class</returns>
        Film GetSingle(Guid filmId);

        /// <summary>
        /// Inserts a Film record
        /// </summary>
        /// <param name="film">An instance of the <see cref="Film"/> class</param>
        /// <returns>An instance of the <see cref="ServiceResult{T}"/> class</returns>
        ServiceResult<Film> Insert(Film film);

        /// <summary>
        /// Saves a Film record
        /// </summary>
        /// <param name="film">An instance of the <see cref="Film"/> class</param>
        /// <returns>An instance of the <see cref="ServiceResult{T}"/> class</returns>
        ServiceResult<Film> Update(Film film);

        /// <summary>
        /// Deletes a Film record
        /// </summary>
        /// <param name="filmId">The Film Id</param>
        /// <returns>An instance of the <see cref="ServiceResult{T}"/> class</returns>
        ServiceResult<Film> Delete(Guid filmId);
    }
}
