// <copyright file="IFilmRepository.cs" company="Stephen J. Joubert">
//    Copyright (c) Stephen J. Joubert All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Common.Interfaces.Repository
{
    using System;
    using System.Collections.Generic;
    using Common.Models;

    /// <summary>
    /// Film Repository interface
    /// </summary>
    public interface IFilmRepository
    {
        /// <summary>
        /// Gets all Film records
        /// </summary>
        /// <returns>A list of <see cref='Film'/> instances</returns>
        List<Film> Get();

        /// <summary>
        /// Gets a Film record
        /// </summary>
        /// <param name="filmId">The Film Id</param>
        /// <returns>An instance of the <see cref='Film'/> class</returns>
        Film GetSingle(Guid filmId);

        /// <summary>
        /// Adds a new Film record
        /// </summary>
        /// <param name="film">An instance of the <see cref="Film"/> class</param>
        /// <returns>True if successful</returns>
        bool Insert(Film film);

        /// <summary>
        /// Updates a Film record
        /// </summary>
        /// <param name="film">An instance of the <see cref="Film"/> class</param>
        /// <returns>True if successful</returns>
        bool Update(Film film);

        /// <summary>
        /// Deletes a Film record
        /// </summary>
        /// <param name="filmId">The Film Id</param>
        /// <returns>True if successful</returns>
        bool Delete(Guid filmId);
    }
}
