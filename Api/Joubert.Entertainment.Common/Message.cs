// <copyright file="Message.cs" company="Stephen J. Joubert">
//     Copyright (c)Stephen J. Joubert. All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Common
{
    /// <summary>
    /// Message class
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Gets or sets the field name
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the message text
        /// </summary>
        public string MessageText { get; set; }
    }
}