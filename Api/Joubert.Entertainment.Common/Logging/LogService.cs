// <copyright file="LogService.cs" company="Stephen J. Joubert">
//     Copyright (c)Stephen J. Joubert. All rights reserved.
// </copyright>

namespace Joubert.Entertainment.Common.Logging
{
    using Interfaces.Logging;
    using global::NLog;

    /// <summary>
    /// Log Service for NLog
    /// </summary>
    public class LogService : ILogService
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogService"/> class
        /// </summary>
        public LogService()
        {
            this._logger = LogManager.GetCurrentClassLogger();
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an instance of the <see cref=""/> class
        /// </summary>
        private Logger _logger { get; }

        #endregion

        #region Public Methods

        /// <inheritdoc/>
        public void Fatal(string message)
        {
            this._logger.Fatal(message);
        }

        /// <inheritdoc/>
        public void Fatal(string message, params object[] args)
        {
            message = string.Format(message, args);
            this._logger.Fatal(message);
        }

        /// <inheritdoc/>
        public void Error(string message)
        {
            this._logger.Error(message);
        }

        /// <inheritdoc/>
        public void Error(string message, params object[] args)
        {
            message = string.Format(message, args);
            this._logger.Error(message);
        }

        /// <inheritdoc/>
        public void Warning(string message)
        {
            this._logger.Warn(message);
        }

        /// <inheritdoc/>
        public void Warning(string message, params object[] args)
        {
            message = string.Format(message, args);
            this._logger.Warn(message);
        }

        /// <inheritdoc/>
        public void Information(string message)
        {
            this._logger.Info(message);
        }

        /// <inheritdoc/>
        public void Information(string message, params object[] args)
        {
            message = string.Format(message, args);
            this._logger.Info(message);
        }

        /// <inheritdoc/>
        public void Debug(string message)
        {
            this._logger.Debug(message);
        }

        /// <inheritdoc/>
        public void Debug(string message, params object[] args)
        {
            message = string.Format(message, args);
            this._logger.Debug(message);
        }

        /// <inheritdoc/>
        public void Verbose(string message)
        {
            this._logger.Trace(message);
        }

        /// <inheritdoc/>
        public void Verbose(string message, params object[] args)
        {
            message = string.Format(message, args);
            this._logger.Trace(message);
        }

        #endregion
    }
}